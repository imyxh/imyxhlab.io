---
layout: default
title: imyxh's blog
---

博客
====

Welcome to my blog. If you would like to subscribe via an RSS/Atom reader, the
link is <https://imyxh.net/blog/feed.xml>.

All of my content under imyxh.net/blog/ is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

{% for post in site.posts %}
  - [{{ post.date | date: "%Y-%m-%d" }}: {{ post.title }}]({{ post.url | relative_url }})
{% endfor %}

