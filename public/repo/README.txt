imyxh.net/repo
==============

This is a little repository for the pacman utility on Archlinux. Many packages
in the Arch repositories continue to be out of date, so I keep some stable
releases of some packages here.

Mostly, though, it's just a little learning experiment on making and hosting a
package repository via GitLab pages.

Note that currently, these packages aren't signed by anyone. You should probably
only download these via HTTPS. :)


Package list
------------

I am currently packaging only for the x86 architecture, in 64-bit. Packages
include:

  - python-r2pipe 4.3.0
  - radare2 4.3.1
  - radare2-cutter 1.10.2


Installing
----------

To source this repository, add the following lines to your /etc/pacman.conf.

```
[imyxh]
Server = https://imyxh.net/repo/
SigLevel = Optional
```


Notes to self
-------------

Adding packages can be done by copying them over and (for example) running:

```
repo-add -R imyxh.db.tar.xz radare2/radare2-4.2.1-1-x86_64.pkg.tar.xz
ln -s radare2/radare2-4.2.1-1-x86_64.pkg.tar.xz .
```

