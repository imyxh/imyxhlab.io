#!/bin/sh
set -e

URL="https://gitlab.com/imyxh/cfg.git"

cat <<-INTRO

This script will clone from gitlab.com/imyxh/cfg to create a bare Git repo
populating the current working directory. This tool is pretty much written for
the sole purpose of allowing me (imyxh) to easily download my dotfiles. Feel
free to adapt it, but be sure to change the git URL!

If you want to be sure no files are overwritten, I recommend you create a new
directory and execute that script within there.

INTRO
read -p "Continue? (y/N) " yn
case $yn in 
	[yY]|yes)
		echo ".cfg" >> "${PWD}/.gitignore"
		git clone --bare "$URL" "${PWD}/.cfg"
		git --git-dir="$PWD/.cfg" --work-tree="$PWD" checkout
		exit 0
		;;
	[nN]|no)
		exit 0
		;;
esac

exit 1

