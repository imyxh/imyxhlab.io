$(document).ready(function(){

    $('.expandblock').accordion({
        active: false,
        animate: {
            duration: 128
        },
        collapsible: true,
    });

    $(".rslides").responsiveSlides({
        auto: true,
        speed: 200,
        timeout: 4000,
        pager: true,
        nav: true,
        pause: true,
        prevText: "",
        nextText: "",
        namespace: "transparent-btns"
    });

});

